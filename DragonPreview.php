<?php

class DragonPreviewHooks
{
    public static function onParserFirstCallInit(Parser $parser)
    {
        $parser->setFunctionHook('dragon_preview', [self::class, 'render_dragon_preview']);
    }

    function parse_options($params)
    {
        return array_reduce(
            array_map(
                function($x){
                    return array_map("trim", explode("=", $x));
                },
                array_slice($params, 2)
            ),
            function ($result, $item) {
                $result[$item[0]] = $item[1];
                return $result;
            },
            []
        );
    }

    function pop_option(&$options, $name, $default=null)
    {
        if ( !isset($options[$name]) )
            return $default;

        $val = $options[$name];
        unset($options[$name]);
        return $val;
    }

    // Render the output of {{#dragon_preview:}}.
    public static function render_dragon_preview(Parser $parser)
    {
        $options = self::parse_options(func_get_args());

        $name = self::pop_option($options, "name", "");
        $size = self::pop_option($options, "size", "128");

        $url = Title::newFromText("Special:Dragon_Preview.svg")->getLinkURL() . "?" . http_build_query($options);
        $output = Html::element("img", ["src"=>$url, "title"=>$name, "alt"=>$name, "width"=>$size, "height"=>$size]);
        return [ $output, 'noparse' => true, 'isHTML' => true ];
    }
}


class DragonPreviewForm extends SpecialPage
{
    function __construct()
    {
        parent::__construct( 'dragonpreviewform' );
    }

    function execute( $par )
    {
        $output = $this->getOutput();
        $this->setHeaders();

        $svg_url = Title::newFromText("Special:Dragon_Preview.svg")->getLinkURL();

        $output->addHTML("<form id='colors'><table>");
        $dp = new DragonPreview();


        $output->addHTML("<tr>");
        $output->addHTML("<td><label for='input_name'>name</label></td>");
        $name = htmlspecialchars($_GET["name"]??"");
        $output->addHTML("<td><input type='text' value='$name' id='input_name' name='name' /></td>");
        $output->addHTML("</tr>");

        $output->addHTML("<tr>");
        $output->addHTML("<td><label for='input_size'>size</label></td>");
        $size = htmlspecialchars($_GET["size"]??"128");
        $output->addHTML("<td><input type='number' value='$size' id='input_size' name='size' /></td>");
        $output->addHTML("</tr>");

        foreach ( array_keys($dp->defaults) as $name )
        {
            $output->addHTML("<tr>");
            $output->addHTML("<td><label for='input_$name'>$name</label></td>");
            $col = $dp->get_parameter($name);
            $output->addHTML("<td><input type='color' value='$col' id='input_$name' name='$name' /></td>");
            $output->addHTML("</tr>");
        }

        $output->addHTML("<tr><td colspan='2'>-</td></tr>");

        foreach ( array_keys($dp->groups) as $name )
        {
            $output->addHTML("<tr>");
            $output->addHTML("<td><label for='input_$name'>$name</label></td>");
            $output->addHTML("<td>");
            $col = $dp->get_parameter($name);
            $output->addHTML("<input type='text' value='$col' id='input_$name' name='$name' size='7' />");
            $output->addHTML("<input type='checkbox' onchange='document.getElementById(\"input_$name\").setAttribute(\"type\", this.checked ? \"color\" : \"text\");' />");
            $output->addHTML("</td>");
            $output->addHTML("</tr>");
        }

        $output->addHTML("</table>");
        $output->addHTML("<div><img alt='preview' id='img_preview' src='$svg_url' /><p id='text_preview'></p></div>");
        $output->addHTML("</form>");

        ob_start();
        ?>
        <style>
            form#colors
            {
                font-family: monospace;
                display: flex;
            }
            form#colors input[type=text], form#colors input[type=color], form#colors input[type=number]
            {
                height: 32px;
                width: 74px;
                box-sizing: border-box;
                font-family: monospace;
            }
            #text_preview
            {
                font-family: monospace;
                white-space: pre-wrap;
            }
        </style>
        <script>
            let preview = document.getElementById("img_preview");
            let text_preview = document.getElementById("text_preview");
            let form = preview.closest("#colors");
            let orig_uri = preview.src;

            function update_preview()
            {
                var data = new FormData(form);
                var query = '?' + (new URLSearchParams(data)).toString();
                preview.src = orig_uri + query;

                var text = "{{#dragon_preview:\n";
                for ( let item of data.entries() )
                {
                    if ( item[1] )
                        text += "|" + item[0] + " = " + item[1]+"\n";
                }
                text += "}}\n";
                text_preview.innerText = text;

                var url = new URL(window.location.href);
                url.search = query;
                history.replaceState(data, "", url.toString());
            }

            for ( let input of form.querySelectorAll("input") )
            {
                input.addEventListener("input", update_preview);
            }

            update_preview();
        </script>
        <?php
        $output->addHTML(ob_get_clean());
    }

    function getGroupName()
    {
        return 'media';
    }
}

class DragonPreviewGenerator extends SpecialPage
{
    function __construct()
    {
        parent::__construct( 'dragonpreviewgenerator' );
    }

    function execute( $par )
    {
        $output = $this->getOutput();

        $output->setArticleBodyOnly(true);
        $config = new HashConfig(["MimeType" => "image/svg+xml"]);
        $output->getContext()->setConfig(new MultiConfig([$config, $output->getConfig()]));

        $dp = new DragonPreview();
        $svg = $dp->svg();
        $output->clearHtml();
        $output->addHtml($svg->saveXML($svg));
    }

    function getGroupName()
    {
        return 'media';
    }
}

class DragonPreview
{
    public $defaults = [
        "color-main" => "#000",
        "color-eye"  => "#fff",
        "color-horn" => "#3a3c3f",
    ];

    function __construct($file="preview.svg", $params=null)
    {
        $this->img_path = __dir__ . "/assets/";
        $this->input = new DomDocument();
        $this->input->load("{$this->img_path}preview.svg");

        $this->params = $params ?? $_GET;


        $xpath = new DomXPath($this->input);

        $this->groups = [];

        foreach ( $xpath->query("//*[@color-group]") as $obj )
        {
            $grp = $obj->getAttribute("color-group");
            if ( isset($this->groups[$grp]) )
            {
                $this->groups[$grp][] = $obj;
            }
            else
            {
                $this->groups[$grp] = [$obj];
            }
            $this->update_style($obj, ["fill" => "transparent", "display" => "none"]);
        }

        ksort($this->groups);

        $this->main_groups = [];
        foreach ( $xpath->query("//*[@color-group-primary]") as $obj )
        {
            $grp = $obj->getAttribute("color-group-primary");
            if ( isset($this->main_groups[$grp]) )
            {
                $this->main_groups[$grp][] = $obj;
            }
            else
            {
                $this->main_groups[$grp] = [$obj];
            }
        }
    }

    private function update_style($element, $rules)
    {
        # Build the array
        $chunks = explode(";", $element->getAttribute("style"));
        $array = [];
        foreach ( $chunks as $chunk )
        {
            if ( !$chunk )
                continue;
            list($name, $value) = explode(":", $chunk);
            $array[trim($name)] = trim($value);
        }

        # Update
        $array = array_replace($array, $rules);

        # Put back into a string
        $style_str = "";
        foreach ( $array as $name => $value )
            $style_str .= "$name:$value;";
        $element->setAttribute("style", $style_str);
    }

    function color($color)
    {
        if ( $color )
        {
            try {
                $col = (new ImagickPixel($color))->getColor();

                if ( $col["a"] )
                    return sprintf("#%02x%02x%02x", $col['r'], $col['g'], $col['b']);
            } catch(Exception $e) {}
        }
        return "";
    }

    function get_parameter($param)
    {
        if ( isset($this->params[$param]) && $this->params[$param] )
            return $this->color($this->params[$param]);
        if ( isset($this->defaults[$param]) && $this->defaults[$param] )
            return $this->color($this->defaults[$param]);
        return "";
    }

    private function process_list($list, $prefix)
    {
        foreach ( $list as $color_group => $elements )
        {
            $col = $this->get_parameter("$prefix$color_group");
            if ( $col )
            {
                foreach ( $elements as $e )
                    $this->update_style($e, ["fill" => $col, "display" => "inline"]);
            }
        }
    }

    function svg()
    {
        $this->process_list($this->main_groups, "color-");
        $this->process_list($this->groups, "");
        return $this->input;
    }

}
