<?php

$magicWords['en'] = [
   'dragon_preview' => [ 0, 'dragon_preview' ],
];

$specialPageAliases['en'] = [
    'dragonpreviewform' => [ 'Dragon Preview Form', 'Dragon Preview Form' ],
    "dragonpreviewgenerator" => [ 'Dragon Preview.svg', 'Dragon Preview.svg' ],
];
